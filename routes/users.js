var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/obtener', function(req, res, next) {
  res.send('respond from obtener');
});

module.exports = router;
